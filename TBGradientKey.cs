﻿/// Gradient Master, © 2016 Thinking Bacon LLC
/// Support Contact: support@thinkingbacon.com

using System;
using UnityEngine;

/// <summary>
/// TBGradientKey class stores the color and time values of a gradient key.
/// </summary>
[Serializable]
public class TBGradientKey : IEquatable<TBGradientKey>
{
    [SerializeField]
    private float _time;

    [SerializeField]
    private Color _color;

    /// <summary>
    /// Time value of the key within the gradient.  Float value between 0 and 1.  If the value is outside of 0 and 1, it is clamped.
    /// </summary>
    public float Time
    {
        set
        {
            _time = Mathf.Clamp01(value);
        }
        get
        {
            return _time;
        }
    }

    /// <summary>
    /// UnityEngine Color value of the key.
    /// </summary>
    public Color Color
    {
        get
        {
            return _color;
        }
        set
        {
            _color = value;
        }
    }

    /// <summary>
    /// Creates a new TBGradientKey with a time and color.
    /// </summary>
    /// <param name="time">Time value of the key within the gradient, float value between 0 and 1</param>
    /// <param name="color">UnityEngine Color value of the key.</param>
    public TBGradientKey(float time, Color color)
    {
        Time = time;
        Color = color;
    }

    /// <summary>
    /// Impliemnts IEquatable Equals.  Tests to see if two keys are equal to eachother.
    /// </summary>
    /// <param name="other">Other key to compare.</param>
    /// <returns>Returns True is keys are euqal, false if not.</returns>
    public bool Equals(TBGradientKey other)
    {
        if(_time.Equals(other.Time))
        {
            if( Color.Equals(other.Color))
                return true;            
        }

        return false;
    }
}
