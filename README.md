# Unity-Gradient-Texture #

This is a 2D texture made from a set of gradients, for use with [gradient mapping](http://technical-eden.blogspot.com/2012/01/gradient-mapping-awesome-way-to-get.html) in Unity 3D. It is a part of a Unity extension called [Gradient Master](http://www.markpetroart.com/unity-tools/gradient-master/), coming soon to the Unity Asset Store.  It is compatible with Unity 5.2 and newer.

## TBGradientTexture ##

This class contains a collection of TBGradients, and uses them to create a Texture2D with those gradients.  Gradients are ordered by height,

## TBGradient ##

This class represents a 1D color gradient.  It contains a collection of TBGradientKeys which make up the gradient.  
A gradient contains a height value, for position within a Texture2D, and a width value.
Keys are ordered by time for correct color evaluation.

## TBGradientKey ##

This class represents a gradient key.  A key contains a time value, for it's position within a gradient, and a color.

# Example Usage #

```
#!C#
//Create a gradient
TBGradient gradient = new TBGradient();

//Add keys to the gradient
gradient.AddKey(new TBGradientKey(0.0f, Color.red));
gradient.AddKey(new TBGradientKey(0.5f, Color.green));
gradient.AddKey(new TBGradientKey(1.0f, Color.blue));

//Create a gradient texture
TBGradientTexture gradienttexture = new TBGradientTexture(1024,1024);

//Add gradient to the texture
gradienttexture.SetGradient(0, gradient);

//Apply to rebuild the Texture
gradienttexture.Apply();

```

# Example Gradient Map #

This is am example of a TBGradientTexture in the Gradient Master editing window(not included):

![GMWindow.PNG](https://bitbucket.org/repo/7dxGg7/images/2061762403-GMWindow.PNG)