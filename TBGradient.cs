﻿/// Gradient Master, © 2016 Thinking Bacon LLC
/// Support Contact: support@thinkingbacon.com

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// TBGradient, a 1D collection of TBGradientKeys and functions to retrieve the correct color at a specific time within the gradient.
/// </summary>
[Serializable]
public class TBGradient : IEquatable<TBGradient>
{
    [SerializeField]
    private List<TBGradientKey> _keys;

    [SerializeField]
    private float _height;

    [SerializeField]
    private float _width;

    /// <summary>
    /// The height value of the TBGradient in a TBGradientTexture
    /// </summary>
    public float Height
    {
        set
        {
            _height = Mathf.Clamp01(value);
        }
        get
        {
            return _height;
        }
    }

    /// <summary>
    /// The width value of the TBGradient in a TBGradientTexture
    /// </summary>
    public float Width
    {
        set
        {
            _width = Mathf.Clamp01(value);             
        }
        get
        {
            return _width;
        }
    }

    /// <summary>
    /// Constructor, creates a new TBGradient with option for default TBGRadientKeys.
    /// </summary>
    /// <param name="initialize">True to initialize with a default TBGRadient, otherwise add no keys.</param>
    public TBGradient(bool initialize = false)
    {
        _keys = new List<TBGradientKey>();
        if(initialize)
        {
            _keys.Add(new TBGradientKey(0f, Color.black));
            _keys.Add(new TBGradientKey(1f, Color.white));
            _height = 1f;
            _width = 0.1f;
        }        
    }

    /// <summary>
    /// Constructor, create a new TBGradient from an existing TBGradient. 
    /// </summary>
    /// <param name="gradient"></param>
    public TBGradient(TBGradient gradient)
    {
        _keys = new List<TBGradientKey>(gradient.GetKeys());
        _width = gradient.Width;
        _height = gradient.Height;
    }

    /// <summary>
    /// Add a new key to the gradient.  Must be a unique key, Gradients cannot have duplicate keys.
    /// </summary>
    /// <param name="time">Time value for the new key.</param>
    /// <param name="color">Color value for the new key.</param>
    public void AddKey(float time, Color color)
    {
        AddKey(new TBGradientKey(time, color));
    }

    /// <summary>
    /// Add a new key to the gradient.  Must be a unique key, Gradients cannot have duplicate keys.
    /// </summary>
    /// <param name="key">TBGradientKey to add.</param>
    public void AddKey(TBGradientKey key)
    {
        if (_keys.Contains(key))
            Debug.LogWarning("Gradient already contains an identical key, cannot have duplicate keys.");
        else
        {
            _keys.Add(key);
            _keys = _keys.OrderBy(x => x.Time).ToList();
        }
    }

    /// <summary>
    /// Removes a key at a specific index.
    /// </summary>
    /// <param name="index">Index of TBGradientKey to remove.</param>
    public void RemoveKey(int index)
    {
        _keys.RemoveAt(index);
    }

    /// <summary>
    /// Sets the time value for the key at a specific index
    /// </summary>
    /// <param name="index">Index of key to change.</param>
    /// <param name="time">New time value for key.</param>
    public void SetKeyTime(int index, float time)
    {
        if (index >= 0 && index < _keys.Count)
        {
            _keys[index].Time = time;
            _keys = _keys.OrderBy(x => x.Time).ToList();
        }
        else
        {
            Debug.LogWarning("Index '" + index.ToString() + "' is out of range for gradient.");
        }
    }

    /// <summary>
    /// Sets the color value for a key at a specific index.
    /// </summary>
    /// <param name="index">Index of the key to change.</param>
    /// <param name="color">New color value for key.</param>
    public void SetKeyColor(int index, Color color)
    {
        if (index >= 0 && index < _keys.Count)
        {
            _keys[index].Color = color;
        }
        else
        {
            Debug.LogWarning("Index '" + index.ToString() + "' is out of range for gradient.");
        }
    }

    /// <summary>
    /// Sets the time and color values for a key at a specific index.
    /// </summary>
    /// <param name="index">Index of the key to change.</param>
    /// <param name="time">New time value for key.</param>
    /// <param name="color">New color value for key.</param>
    public void SetKey(int index, float time, Color color)
    {
        SetKeyColor(index, color);
        SetKeyTime(index, time);
    }

    /// <summary>
    /// Sets the TBGradientKey at a specific index.
    /// </summary>
    /// <param name="index">Index of the key to change.</param>
    /// <param name="key">TBGradientKey to set.</param>
    public void SetKey(int index, TBGradientKey key)
    {
        if (_keys.Contains(key))
            Debug.LogWarning("Gradient already contains an identical key, cannot have duplicate keys.");
        else
        {
            if (index >= 0 && index < _keys.Count)
            {
                _keys[index] = key;
            }
            else
            {
                Debug.LogWarning("Index '" + index.ToString() + "' is out of range for gradient.");
            }
        }
    }

    /// <summary>
    /// Sets the array of keys.
    /// </summary>
    /// <param name="keys">Array of TBGradientKeys to set.</param>
    public void SetKeys(TBGradientKey[] keys)
    {
        _keys = new List<TBGradientKey>(keys);
        _keys = _keys.OrderBy(x => x.Time).ToList();
    }

    /// <summary>
    /// Returns a TBGradientKey at a specific index.
    /// </summary>
    /// <param name="index">Index of key to return.</param>
    /// <returns>TBGradientKey at index.</returns>
    public TBGradientKey GetKey(int index)
    {
        if (index >= 0 && index < _keys.Count)
            return _keys[index];
        else
        {
            Debug.LogWarning("Index '" + index.ToString() + "' is out of range for gradient.");
            return null;
        }
    }

    /// <summary>
    /// Returns an array of all TBGradientKeys in the gradient.
    /// </summary>
    /// <returns>Array of TBGradientKeys in the gradient.</returns>
    public TBGradientKey[] GetKeys()
    {
        return _keys.ToArray();
    }

    /// <summary>
    /// Number of keys in the gradient.
    /// </summary>
    public int keyCount
    {
        get
        {
            return _keys.Count;
        }
    }

    /// <summary>
    /// Returns the interpolated Color value at a specific time in the gradient.
    /// </summary>
    /// <param name="time">Time to evaluate a color for.</param>
    /// <returns>Color at time in the gradient.</returns>
    public Color Evaluate(float time)
    {
        if (_keys.Count < 2)
            return _keys[0].Color;
        else if (_keys.Count < 1)
            return Color.white;
        else
        {
            int left = 0;
            int right = 1;

            for (int i = 1; i < _keys.Count(); i++)
            {
                float keytime = _keys[i].Time;

                if (keytime < time)
                    left = i;

                if (keytime >= time)
                {
                    right = i;
                    break;
                }
            }

            float localtime = Mathf.Clamp01((time - _keys[left].Time) / (_keys[right].Time - _keys[left].Time));          

            return Color.Lerp(_keys[left].Color, _keys[right].Color, localtime);
        }
    }

    /// <summary>
    /// Reorders keys by time.
    /// </summary>
    public void ResetOrder()
    {
        _keys = _keys.OrderBy(x => x.Time).ToList();
    }

    /// <summary>
    /// Impliemnts IEquatable Equals.  Tests to see if two gradients are equal to eachother.
    /// </summary>
    /// <param name="other">Other gradient to compare.</param>
    /// <returns>Returns True is gradients are euqal, false if not.</returns>
    public bool Equals(TBGradient other)
    {
        if(_height.Equals(other.Height))
        {
            if(_keys.Count.Equals(other.keyCount))
            {
                for(int i = 0; i < _keys.Count(); i++)
                {
                    if (_keys[i].Equals(other.GetKey(i)))
                        continue;
                    else
                        return false;
                }
                return true;
            }
        }
        return false;

    }
}
