﻿/// Gradient Master, © 2016 Thinking Bacon LLC
/// Support Contact: support@thinkingbacon.com

using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

/// <summary>
/// TBGradientTexture, a Texture2D created from a prodecural set of TBGradients.
/// </summary>
[Serializable]
public class TBGradientTexture 
{

    [SerializeField]
    private Texture2D _texture;

    [SerializeField]
    private List<TBGradient> _gradients;

    /// <summary>
    /// Width of the texture.  (Read Only)
    /// </summary>
    public int width
    {
        get
        {
            if (_texture == null)
                return 0;
            return _texture.width;
        }
    }   

    /// <summary>
    /// Height of the texture.  (Read Only)
    /// </summary>
    public int height
    {
        get
        {
            if (_texture == null)
                return 0;
            return _texture.height;
        }

    }

    /// <summary>
    /// Texture2D format.  textureFormat enum. (Read Only)
    /// </summary>
    public TextureFormat format
    {
        get
        {
            if (_texture == null)
                return TextureFormat.ARGB32;
            return _texture.format;
        }
    }

    /// <summary>
    /// Anisotropic filtering level of the texture.
    /// </summary>
    public int anisoLevel
    {
        get
        {
            return _texture.anisoLevel;
        }
        set
        {
            _texture.anisoLevel = value;
        }
    }

    /// <summary>
    /// Texture Wrap Mode of the texture. TextureWrapMode enum.
    /// </summary>
    public TextureWrapMode wrapMode
    {
        get
        {
            return _texture.wrapMode;
        }
        set
        {
            _texture.wrapMode = value;
        }
    }

    /// <summary>
    /// Filter Mode of the texture.  FilterMode enum.
    /// </summary>
    public FilterMode filterMode
    {
        get
        {
            return _texture.filterMode;
        }
        set
        {
            _texture.filterMode = value;
        }
    }

    /// <summary>
    /// Applies changes to the TBGradientTexture.  Rebuilds the Texture2D with all changes to size and gradients.
    /// </summary>
    public void Apply()
    {
        RebuildTexture();
    }

    /// <summary>
    /// From Texture2D.
    /// Encodes the Texture2D to a PNG btye array.
    /// </summary>
    /// <returns>Byte array encoded as a PNG</returns>
    public byte[] EncodeToPNG()
    {
        return _texture.EncodeToPNG();
    }

    /// <summary>
    /// From Texture2D.
    /// Encodes the Texture2D to a JPG byte array.
    /// </summary>
    /// <returns>Byte array encoded as a JPG.</returns>
    public byte[] EncodeToJPG()
    {
        return _texture.EncodeToJPG();
    }

    /// <summary>
    /// From Texture2D.
    /// Returns pixel color at coordinates (x, y).
    /// </summary>
    /// <param name="x">int X location of pixel color to return.</param>
    /// <param name="y">int Y location of pixel color to return.</param>
    /// <returns>Color of pixel at position (x,y)</returns>
    public Color GetPixel(int x, int y)
    {
        return _texture.GetPixel(x, y);
    }

    /// <summary>
    /// From texture2D.
    /// Returns filtered pixel color at normalized coordinates (u, v)
    /// </summary>
    /// <param name="u">float U texture coordinate of color to return.</param>
    /// <param name="v">float U texture coordinate of color to return.</param>
    /// <returns>Color at texure coordinate (u,v)</returns>
    public Color GetPixelBilinear(float u, float v)
    {
        return _texture.GetPixelBilinear(u, v);
    }

    /// <summary>
    /// From texture2D. 
    /// Get a block of pixel colors. This function returns an array of pixel colors of the whole mip level of the texture.
    /// </summary>
    /// <param name="miplevel">Mipmap level to sample</param>
    /// <returns>Array of pixels for the texture</returns>
    public Color[] GetPixels(int miplevel = 0)
    {
        return _texture.GetPixels(miplevel);
    }

    /// <summary>
    /// From Texture2D.
    /// Get a block of pixel colors in Color32 format.  This function returns an array of pixel colors of the whole mip level of the texture.
    /// </summary>
    /// <param name="miplevel">Mipmap level to sample</param>
    /// <returns>Array of pixels in Color32 format</returns>
    public Color32[] GetPixels32(int miplevel = 0)
    {
        return _texture.GetPixels32(miplevel);
    }

    /// <summary>
    /// Number of TBGradients in the texture.
    /// </summary>
    public int Count
    {
        get
        {
            return _gradients.Count;
        }
    }

    /// <summary>
    /// Returns the Texture2D generated from the gradients.  READ-ONLY
    /// </summary>
    public Texture2D Texture
    {
        get
        {
            return _texture;
        }
    }

    /// <summary>
    /// Adds a gradient to the texture.
    /// </summary>
    /// <param name="gradient">Gradient to add</param>
    public void AddGradient(TBGradient gradient)
    {
        if (_gradients.Contains(gradient))
        {
            Debug.LogWarning("Texture already contains identical gradient, cannot have duplicate gradients.");
            return;
        }

        _gradients.Add(gradient);
        _gradients = _gradients.OrderBy(x => x.Height).ToList();

    }

    /// <summary>
    /// Returns an array of all TBGradients in the texture.
    /// </summary>
    /// <returns>TBGradient array of current TBGradients</returns>
    public TBGradient[] GetGradients()
    {
        return _gradients.ToArray();
    }

    /// <summary>
    /// Sets a specific TBGradient in the texture;
    /// </summary>
    /// <param name="index">Index of TBGradient to set.</param>
    /// <param name="gradient">New TBGradient to set.</param>
    public void SetGradient(int index, TBGradient gradient)
    {
        if(gradient == null)
        {
            Debug.LogWarning("TBGradient cannot be null.");
            return;
        }
        else if(!IsIndexInRange(index))
        {
            Debug.LogWarning("Index is out of range, cannot set TBGradient.");
            return;
        }

        _gradients[index] = gradient;
    }

    /// <summary>
    /// Sets the gradients used by the texture.
    /// </summary>
    /// <param name="gradients">TBGradient array to set.</param>
    public void SetGradients(TBGradient[] gradients)
    {
        _gradients = new List<TBGradient>(gradients);
    }

    /// <summary>
    /// Returns the gradient as a specific index.
    /// </summary>
    /// <param name="index">Index of the TBGradient to get.</param>
    /// <returns>TBGradient at index</returns>
    public TBGradient GetGradient(int index)
    {
        if (!IsIndexInRange(index))
        {
            Debug.LogWarning("Index is out of range, cannot get TBGradient.");
            return null;
        }

        return _gradients[index];

    }
    
    /// <summary>
    /// Removes a gradient from the texture
    /// </summary>
    /// <param name="index">Gradient index to remove</param>
    public void RemoveGradientAtIndex(int index)
    {
        if (!IsIndexInRange(index))
        {
            Debug.LogWarning("Index " + index + " is out of range.  Cannot remove TBGradient from TBGradientTexture.");
            return;
        }

        _gradients.RemoveAt(index);
            
    }

    /// <summary>
    /// Creates a new TBGradientTexture, initializes the texture with a specific width and height.
    /// </summary>
    /// <param name="width">Texture width</param>
    /// <param name="height">Texture Height</param>
    public TBGradientTexture(int width, int height)
    {
        Initialize(ValidateSize(width), ValidateSize(height), TextureFormat.ARGB32, true);
    }

    /// <summary>
    /// Creates a new TBGradientTexture, initializes the texture with a specific width, height, format and if mipmaps should be generated.
    /// </summary>
    /// <param name="width">Texture width</param>
    /// <param name="height">Texture height</param>
    /// <param name="format">Texture Format</param>
    /// <param name="mipmap">Should it have mipmaps</param>
    public TBGradientTexture(int width, int height, TextureFormat format, bool mipmap)
    {
        Initialize(ValidateSize(width), ValidateSize(height), format, mipmap);
    }

    /// <summary>
    /// Initialized the TBGradientTexure with default values
    /// </summary>
    public void Initialize()
    {
        Initialize(1024, 1024, TextureFormat.ARGB32, true);
    }

    /// <summary>
    /// Initializes the TBGradientTexture.
    /// </summary>
    /// <param name="_width">Texture width</param>
    /// <param name="_height">Texture height</param>
    /// <param name="_format">Texture Format</param>
    /// <param name="_mipmap">Should it have mipmaps</param>
    public void Initialize(int _width, int _height, TextureFormat _format, bool _mipmap)
    {
        _texture = new Texture2D(_width, _height, _format, _mipmap);
        _texture.name = "Gradient Texture 2D";
        _gradients = new List<TBGradient>();
        _gradients.Add(new TBGradient(true));
    }

    /// <summary>
    /// Resizes the texture.
    /// </summary>
    /// <param name="width">New width</param>
    /// <param name="height">new height</param>
    public void Resize(int width, int height)
    {
        _texture.Resize(width, height);
    }

    /// <summary>
    /// Makes sure all gradients are in order by height, and all keys in each gradient are in order by time
    /// </summary>
    public void ResetOrder()
    {
        _gradients = _gradients.OrderBy(x => x.Height).ToList();
        for(int i = 0; i < _gradients.Count; i++)
        {
            _gradients[i].ResetOrder();
        }
    }

    /// <summary>
    /// Rebuilds the gradient Texture2D
    /// </summary>
    private void RebuildTexture()
    {
        if (_texture == null)
            return;

        if (_texture.height != height || _texture.width != width)
            _texture.Resize(width, height);

        ResetOrder();

        for (int y = 0; y < height; y++)
        {
            float heighttime = (float)y / (float)(height - 1);

            int left = 0;
            int right = 1;

            if (_gradients.Count >= 2)
            {
                for (int i = 1; i < _gradients.Count(); i++)
                {
                    if (_gradients[i].Height < heighttime)
                        left = i;

                    if (_gradients[i].Height >= heighttime)
                    {
                        right = i;
                        break;
                    }
                }
            }
            else if (_gradients.Count == 1)
            {
                right = 0;
            }

            TBGradient left_grad = _gradients[left];
            TBGradient right_grad = _gradients[right];

            float adjustedLeft = left_grad.Height + (left_grad.Width * 0.5f);
            float adjustedRight = right_grad.Height - (right_grad.Width * 0.5f);

            float localHtime = Mathf.Clamp01((heighttime - adjustedLeft) / (adjustedRight - adjustedLeft));

            for (int x = 0; x < width; x++)
            {
                float widthtime = (float)x / (float)(width - 1);
                Color bottompixel = left_grad.Evaluate(widthtime);
                Color toppixel = right_grad.Evaluate(widthtime);
                Color pixel = Color.Lerp(bottompixel, toppixel, localHtime);
                _texture.SetPixel(x, y, pixel);
            }
        }

        _texture.Apply();
    }

    /// <summary>
    /// Helper function to make sure a gradient index in in range.
    /// </summary>
    /// <param name="index">Index to check</param>
    /// <returns>True if in range, false if not</returns>
    private bool IsIndexInRange(int index)
    {
        return (index >= 0 && _gradients.Count > 0 && index < _gradients.Count);
    }

    /// <summary>
    /// Clamps the size of the texture between 0 and 4096.
    /// </summary>
    /// <param name="size">Input Size</param>
    /// <returns>Clamped size</returns>
    public static int ValidateSize(int size)
    {
        if (size < 0)
        {
            Debug.LogWarning("Texture size can't be less than 0.");
            return 0;
        }
        else if (size > 4096)
        {
            Debug.LogWarning("Texture size can't be greater than 4096.");
            return 4096;
        }
        else
        {
            return size;
        }
    }
}
